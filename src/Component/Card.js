import React from "react";
import Grid from "@material-ui/core/Grid";
import StoreIcon from "@material-ui/icons/Store";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

function Card(props) {
  return (
    <>
      <Grid container alignItems="center" justify="center">
        <Paper
          class="MuiPaper-root MuiPaper-elevation1 MuiPaper-rounded"
          style={{
            padding: "4px",
            margin: "18px",
            borderRadius: "5px",
            bottom: "0px",
            maxWidth: "360px"
          
           
          }}
        >
          <Grid
            class="MuiGrid-root MuiGrid-container"
            container
            alignItems="center"
            justify="center"
            style={{
              padding: "12px"
            }}
          >
            <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-12">
              <Typography
                style={{
                  margin: "0",
                  fontFamily: "Montserrat",
                  fontSize: "14px",
                  color: "rgb(78, 83, 86)"
                }}
              >
                Kamu Belanja Di :
              </Typography>
            </Grid>
            <Grid
              class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-1"
              style={{
                marginTop: "5px"
              }}
            >
              <StoreIcon
                fontSize="large"
                style={{
                  fontSize: "30px",
                  color: "rgb(135, 202, 254)"
                }}
              />
            </Grid>
            <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-8">
              <Typography
                class="MuiTypography-gutterBottom"
                variant="body2"
                style={{
                  fontSize: "14px",
                  fontFamily: "Montserrat",
                  marginTop: "10px",
                  paddingRight: "0px",
                  paddingLeft: "14px"
                }}
              >
                <b>Pasar Bulu Semarang</b>
              </Typography>
            </Grid>
            <Grid
              class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-2"
              style={{
                marginTop: "5px"
              }}
            >
              <Button
                class="MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedSizeSmall MuiButton-sizeSmall"
                variant="contained"
                style={{
                  backgroundColor: "#F15B5D",
                  color: "#ffffff",
                  fontFamily: "Montserrat",
                  fontSize: "13px"
                }}
              >
                Ganti
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </>
  );
}
export default Card;

import React, { useState, useEffect } from "react";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles"
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    margin: "6px",
    justify:"center",
    marginLeft: "11px",
    marginTop: "5px"
  },
}));

export default function Categories() {
   const [categories, setCategories] = useState([])
   const classes = useStyles();
    useEffect(() => {
        axios
          .get("https://api.tumbasin.id/t/v1/products/categories")
          .then(response => setCategories(response.data));
      }, [])

    return (
     
        <div className={classes.root}>
        <div  style={{ display:"flex", flexWrap:"wrap" }}>
          {categories.slice(0,5).map((category) => (
          <div>
            <Paper className={classes.paper}>
              <img style={{ width: 20 , height:20 }} src={category.image.src}/></Paper>
            <Typography  style={{
                  fontFamily: "Montserrat",
                  fontSize: "11px",
                  marginLeft: "10px",
                  marginBottom: "10px",
                  textAlign: "center"
                }}
              >{category.name}</Typography>
          </div>
          ))}
          </div>
       
        <div style={{display:"flex",flexWrap:"wrap"}}>
        {categories.slice(5,11).map((category) => (
          <div>
            <Paper className={classes.paper}><img style={{width:20,height:20}} src={category.image.src}/></Paper>
            <Typography  style={{
                  fontFamily: "Montserrat",
                  textAlign:"center",
                  fontSize: "11px",
                  marginLeft: "10px"
                }}
              >{category.name}</Typography>
          </div>
          ))}
          </div>
      </div>
    );

}
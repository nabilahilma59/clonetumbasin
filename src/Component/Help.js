import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";


const useStyles = makeStyles((theme) => ({
  User: {
    marginTop: "65px",
    background:
      "linear-gradient(180.97deg, rgb(209, 48, 50) -22.67%, rgb(227, 65, 67) -3.9%, rgb(237, 82, 84) 16.54%, rgb(241, 91, 93) 57.41%)"
  },
  teks: {
    color: "white",
    marginTop: "5px",
    justifyContent:"center"
  }
}));

export default function Help() {
  const classes = useStyles();

  return (
    <>
      <Card className={classes.User} color="secondary">
        <Grid container alignItems="center" justify="center">
          <Typography
            style={{
              fontFamily: "Montserrat"
            }}
            className={classes.teks}
          >
            <p>Hello Users</p>
          </Typography>
          <Grid container alignItems="center" justify="center">
            <Typography
              style={{
                fontFamily: "Montserrat",
                fontSize: "20px"
              }}
              className={classes.teks}
            >
              <p>Anda Memerlukan Bantuan</p>
            </Typography>
          </Grid>
        </Grid>
        <AppBar position="absolute" color="transparent">
          <Toolbar>
            <Typography
              style={{
                fontFamily: "Montserrat"
              }}
              className={classes.menuButton}
            >
              <b>Bantuan</b>
            </Typography>
          </Toolbar>
        </AppBar>
      </Card>
      <br />
      <div>
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          <b>Tentang Kami</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Apa Tumbasin.id?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          <b>Operasional</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Pasar mana saja yang bekerja sama dengan Tumbasin.id?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Operasional Tumbasin.id sudah ada dikota mana saja?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          <b>Harga dan Transaksi</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Berapa ongkos kirimnya?
        </Typography>{" "}
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Kapan saja dapat menerima pengembalian uang dari Tumbasin.id?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          <b>Pesanan</b>
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Seberapa cepat Tumbasin.id mengantarkan pesanan?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Apa yang dimaksud waktu dengan pengantaran?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana jika stok barang yang saya pesan habis?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Apakah harga barang di Tumbasin.id berbeda dengan harga di Pasar
          Tradisional?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "13px",
            alignItems: "center"
          }}
        >
          Bagaimana saya mengecek status pesanan saya?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana cara mengedit atau membatalkan pesanan
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Apa kebijakan Tumbasin.id terkait pembatalan pesanan?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana jika saya ingin melaporkan masalah terhadap pesanan saya?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana melihat struk pembelanjaan saya?
        </Typography>{" "}
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana jika saya ingin mengembalikan kantong belanja Tumbasin.id ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Bagaimana jika saya ingin meretur barang ?
        </Typography>{" "}
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Siapa yang akan memilih pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Siapa yang akan mengantarkan pesanan saya ?
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Saya mempunyai pertanyaan lebih lanjut Tentang Tumbasin.id!
        </Typography>
        <hr />
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "15px"
          }}
        >
          Masih <b>butuh bantuan</b> atau <b>punya pertanyaan lain</b> yang
          ingin ditanyakan? <font color="red">Hubungi kami</font>
        </Typography>
        <hr />
        <div>
          <img
            style={{
              width: "20px",
              height: "30px"
            }}
            src="https://tumbasin.id/static/media/info.f0b746a7.svg"
          />
          <Grid container justify="center">
            <div>
              <Typography
                style={{
                  color: "rgb(137, 139, 140)",
                  maxWidth: "700px",
                  maxHeight: "200px",
                  fontFamily: "Montserrat",
                  fontSize: "13px",
                  marginBottom: "60px",
                  alignItems: "center"
                }}
              >
                Layanan Pelanggan 24 Jam, Senin s/d Minggu, tidak termasuk Hari
                Libur Nasional.
              </Typography>
            </div>
          </Grid>
        </div>
        <img
          style={{
            // backgroundRepeat: "no-repeat",
            right: "0",
            position: "fixed",
            bottom: "15px",
            marginBottom: "56px"
          }}
          alt=""
          src="https://tumbasin.id/static/media/wa.4c0c2a92.svg"
        />
      </div>
    </>
  );
}

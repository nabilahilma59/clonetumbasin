import React from "react";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PersonIcon from "@material-ui/icons/Person";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  actionItem: {
    "&$selected": {
      color: "#F15B5D"
    }
  },
  selected: {}
}));

function BottomNav() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  return (
    <>
      <Grid
        container
        alignItems="center"
        justify="center"
        style={{
          height: "40px",
          width: "100%"
        }}
      >
        <BottomNavigation
          class="MuiBottomNavigation-root"
          style={{
            width: "360px",
            position: "fixed",
            boxShadow: "0px 0px 2px #9e9e9e",
            bottom: "0",
            fontFamily: "Montserrat"
          }}
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
          showLabels
          className={classes.root}
        >
          <BottomNavigationAction
            // class="MuiBottomNavigationAction-root MuiButtonBase-root MuiBottomNavigationAction-wrapper"
            classes={{ root: classes.actionItem, selected: classes.selected }}
            component={Link}
            to="/"
            label="Belanja"
            icon={<StorefrontIcon />}
          />
          <BottomNavigationAction
            // class="MuiBottomNavigationAction-root MuiButtonBase-root MuiBottomNavigationAction-wrapper"
            classes={{ root: classes.actionItem, selected: classes.selected }}
            component={Link}
            to="/login"
            label="Transaksi"
            icon={<ReceiptIcon />}
          />
          <BottomNavigationAction
            // class="MuiBottomNavigationAction-root MuiButtonBase-root MuiBottomNavigationAction-wrapper"
            classes={{ root: classes.actionItem, selected: classes.selected }}
            component={Link}
            to="/help"
            label="Bantuan"
            icon={<LiveHelpIcon />}
          />
          <BottomNavigationAction
            // class="MuiBottomNavigationAction-root MuiButtonBase-root MuiBottomNavigationAction-wrapper"
            classes={{ root: classes.actionItem, selected: classes.selected }}
            component={Link}
            to="/login"
            label="Profile"
            icon={<PersonIcon />}
          />
        </BottomNavigation>
      </Grid>
    </>
  );
}
export default BottomNav;

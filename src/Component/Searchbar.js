import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";


const useStyles = makeStyles((theme) => ({
  root: {
    padding: "4px 20px",
    display: "flex",
    width: "77%",
    maxHeight: "28px",
    marginTop: "20px",
    borderRadius: "50px"

  },
  input: {
    marginLeft: theme.spacing(1)

  },
  iconButton: {
    padding: 1,
    marginLeft: "20px"
  } 

}));

export default function Searchbar() {
  const classes = useStyles();

  return (
    <Grid container justify="center" alignItems="center">
      <Paper className={classes.root}>
        <IconButton
          type="submit"
          className={classes.iconButton}
          aria-label="search"
        >
          <SearchIcon />
        </IconButton>
        <InputBase className={classes.input} placeholder="Search..." />
      </Paper>
    </Grid>
  );
}

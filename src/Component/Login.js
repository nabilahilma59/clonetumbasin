import React from "react";
import Typography from "@material-ui/core/Typography";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";

function Login() {
  return (
    <div align="center">
      <Paper
        align="center"
        justify="center"
        class="MuiPaper-root MuiAppBar-root MuiAppBar-positionStatic MuiAppBar-colorPrimary MuiPaper-elevation0"
        style={{
          backgroundColor: "#ffffff",
          color: "#000000",
          top: "0px",
          width: "100%",
          maxWidth: "360px",
          maxHeight: "120px",
          marginLeft: "120px"
        }}
      >
        <AppBar elevation={0} position="absolute" color="transparent">
          <Toolbar variant="dense">
            <IconButton
              component={Link}
              to="/"
              edge="start"
              style={{
                color: "#fc4c4e"
              }}
            >
              <ArrowBackIosRoundedIcon />
            </IconButton>
            <Typography
              color="inherit"
              style={{
                fontFamily: "Montserrat",
                fontSize: "16px"
              }}
            >
              <b>Masuk Akun</b>
            </Typography>
          </Toolbar>
        </AppBar>
      </Paper>
      <Grid
        container
        alignItems="center"
        justify="center"
        style={{
          marginTop: "90px"
        }}
      >
        <img
          alt=""
          src={"https://tumbasin.id/static/media/masuk.097ee68d.svg"}
          height="100"
        />
      </Grid>
      <Grid
        container
        alignItems="center"
        justify="center"
        style={{
          marginTop: "30px"
        }}
      >
        <Typography
          style={{
            fontFamily: "Montserrat",
            fontSize: "14px"
          }}
        >
          <b>Masuk</b>
        </Typography>
      </Grid>
      <Typography
        align="center"
        style={{
          fontFamily: "Montserrat",
          fontSize: "12px",
          marginTop: "12px",
          marginBottom: "40px"
        }}
      >
        Nikmati kepuasan dan kenyamanan kualitas <br />
        belanja kebutuhan sehari - hari dengan Tumbasin
      </Typography>
      <Grid container justify="center">
        <Button
          style={{
            backgroundColor: "rgb(66, 133, 244)",
            color: "#ffffff",
            textTransform: "none",
            fontFamily: "Montserrat"
          }}
        >
          <img
            alt=""
            src="https://tumbasin.id/static/media/google.1afb8f6b.svg"
            height="20px"
            style={{
              marginRight: 8
            }}
          />
          Sign up with google
        </Button>
      </Grid>
      <br />
      <Grid container justify="center">
        <Button
          variant="contained"
          size="medium"
          style={{
            backgroundColor: "#fc4c4e",
            color: "#ffffff",
            textTransform: "none",
            fontFamily: "Montserrat"
          }}
        >
          <img
            alt=""
            src="https://tumbasin.id/static/media/emaillain.3fcfe399.svg"
            height="20px"
            style={{
              marginRight: 8
            }}
          />
          Sign up with email
        </Button>
      </Grid>
      <br />
      <br />
      <span
        class="MuiTypography-gutterBottom MuiTypography-alignCenter MuiTypography-displayBlock"
        style={{
          color: "rgb(159, 163, 166)",
          fontFamily: "Montserrat",
          fontSize: "10px"
        }}
      >
        Dengan masuk dan mendaftar, Anda menyetujui
      </span>
      <span
        class="MuiTypography-alignCenter MuiTypography-displayBlock"
        style={{
          color: "rgb(159, 163, 166)",
          fontFamily: "Montserrat",
          fontSize: "10px"
        }}
      >
        <b> Syarat Penggunaan</b> dan<b> Kebijakan Privasi</b>
      </span>
    </div>
  );
}

export default Login;

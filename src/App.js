import  React from "react";
import Login from "./Component/Login";
import BottomNavigation from "./Component/BottomNavigation";
import Home from "./page/Home";
import Help from "./Component/Help";
import Banner from "./Component/Banner";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Category from "./Component/Category";
import Searchbar from "./Component/Searchbar";
import Productspage from "./page/Productspage";


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home/>
           <Productspage/>
           <BottomNavigation />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/help">
          <Help />
          <BottomNavigation /> 
           </Route>
           </Switch>
           </Router>     
         
  );
}

export default App;

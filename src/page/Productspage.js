import React, { useState, useEffect } from 'react';
import axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginRight: "10px",
    justify: "center"
  },
  img: {
    width:100,
    height:100,
    marginLeft: "10px",
    justify: "center"
  },
  text: {
    fontFamily: "Montserrat",
    fontSize: "12px",
    marginLeft: "10px"
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();
  const [products, setProducts] = useState([])
   useEffect(() => {
       axios
         .get("https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true")
         .then(response => setProducts(response.data));
     }, [])

     const [quantity, setQuantity] = useState(0);
     function tambah() {
       setQuantity(quantity + 1);
     }
     function kurang() {
       if (quantity > 0) {
         setQuantity(quantity - 1);
       }
     }

  return (
    <div className={classes.root}>
      {products.slice(0,5).map((product) => (
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <img className={classes.img} src={product.images[0].src}/>
        </Grid>
        <Grid item xs={6}>
        <p className={classes.text}>
          {product.name}
        </p><br/>
        <p className={classes.text}>
          <font color="red">Rp.{product.price}</font>
          <font color="#C7C7C9">/{product.meta_data[0].value}</font>
        </p>
        
        </Grid>
        <Grid item xs={3}>
        <Grid style={{ display: "flex", alignItems: "flex-end" }}>
              {quantity == 0 && (
                <Button
                  style={{
                    fontFamily: "Montserrat",
                    fontSize: "11px",
                    // marginRight: "2px"
                  }}
                  variant="contained"
                  color="rgb(237, 82, 84)"
                  onClick={tambah}
                >
                  Tambahkan
                </Button>
              )}

              {quantity > 0 && (
                <>
                  <Button
                    size="small"
                    style={{ minWidth: 15 }}
                    variant="outlined"
                    onClick={kurang}
                  >
                    -
                  </Button>

                  <Typography
                    style={{
                      marginLeft: "5px"
                    }}
                    variant="body2"
                    color="textSecondary"
                  >
                    {quantity}
                  </Typography>

                  <Button
                    size="small"
                    style={{ minWidth: 15, marginLeft: "5px", marginRight:"5px" }}
                    variant="contained"
                    color="secondary"
                    onClick={tambah}
                  >
                    +
                  </Button>
                </>
              )}
            </Grid>
        </Grid>
      </Grid>
      
      ))}
    </div>
  );
}
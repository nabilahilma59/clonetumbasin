import React from "react";
import Container from '@material-ui/core/Container';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Searchbar from "../Component/Searchbar";
import Card from "../Component/Card";
import Category from "../Component/Category";
import Banner from "../Component/Banner";
import Paper from "@material-ui/core/Paper";
import { BottomNavigation } from "@material-ui/core";

function Home() {
  return (
    <>
     <Grid container alignItems="center" justify="center">
        <Grid
          style={{
            maxWidth: "360px"
          }}
        >
          <div
            style={{
              backgroundImage:
                "url(" +
                "https://tumbasin.id/static/media/appbar.4aa6cb6f.svg" +
                ")",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              maxHeight: "120px",
              top: "0"
            }}
          >
            <Searchbar />
            <Card />
          </div>

          <Paper
            class="MuiPaper-elevation0 MuiPaper-rounded"
            style={{
              borderRadius: "0px",
              minHeight: "550px",
              top: "0px"
            }}
          >
            <div
              style={{
                width: "100%"
              }}
            >
              <Typography
                style={{
                  marginTop: "70px",
                  marginBottom: "4px",
                  marginLeft: "20px",
                  fontFamily: "Montserrat",
                  fontSize: "14px"
                }}
              >
                <b>Telusuri jenis produk</b>
              </Typography>

              <Grid
                container
                alignItems="center"
                class="MuiGrid-root MuiGrid-container"
                style={{
                  marginLeft: "4px",
                  marginTop: "10px",
                  marginBottom: "10px",
                  display: "flex",
                  width: "100%"
                  
                }}
              >
                <Category/>
              </Grid>
              <Banner />
              <Grid
                class="MuiGrid-root MuiGrid-container"
                style={{
                  marginTop: "10px",
                  marginBottom: "10px"
                }}
              >
                <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6">
                  <Typography
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      marginLeft: "11px",
                    marginTop:"10px"
                     
                    }}
                  >
                    <b>Produk Terlaris</b>
                  </Typography>
                </Grid>
                <Grid class="MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6">
                  <Typography
                    align="right"
                    style={{
                      fontFamily: "Montserrat",
                      fontSize: "14px",
                      color: "#F15B5D",
                      marginTop:"10px"
                    }}
                  >
                    Lihat semua
                  </Typography>
                </Grid>
              </Grid>
             
              <BottomNavigation />
            </div>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}
export default Home;
